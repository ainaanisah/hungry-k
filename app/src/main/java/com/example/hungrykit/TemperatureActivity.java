package com.example.hungrykit;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class TemperatureActivity extends AppCompatActivity {

    private ImageButton backBtn;
    private TextView dateTimeTv;

    RecyclerView cat_recyclerView;
    ArrayList<ModelCat> catList;
    AdapterCat adapterCat;

    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temperature);

        backBtn = findViewById(R.id.backBtn);
        dateTimeTv = findViewById(R.id.dateTimeTv);


        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEEE, dd/MMM/yyyy");
        String dateTime = simpleDateFormat.format(calendar.getTime());
        dateTimeTv.setText(dateTime);



    }


}