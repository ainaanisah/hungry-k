package com.example.hungrykit;

public class ModelCat {

    //use same name as in firebase database
    String catID, catName, catType, catWeight, catCategory, catDescription, catImage, uid;

    public ModelCat() {
    }

    public String getCatID() {
        return catID;
    }

    public void setCatID(String catID) {
        this.catID = catID;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public String getCatType() {
        return catType;
    }

    public void setCatType(String catType) {
        this.catType = catType;
    }

    public String getCatWeight() {
        return catWeight;
    }

    public void setCatWeight(String catWeight) {
        this.catWeight = catWeight;
    }

    public String getCatCategory() {
        return catCategory;
    }

    public void setCatCategory(String catCategory) {
        this.catCategory = catCategory;
    }

    public String getCatDescription() {
        return catDescription;
    }

    public void setCatDescription(String catDescription) {
        this.catDescription = catDescription;
    }

    public String getCatImage() {
        return catImage;
    }

    public void setCatImage(String catImage) {
        this.catImage = catImage;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}
