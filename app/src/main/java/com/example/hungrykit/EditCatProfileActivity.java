package com.example.hungrykit;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.util.EventListener;
import java.util.HashMap;

public class EditCatProfileActivity extends AppCompatActivity {

    private ImageButton backBtn;
    private ImageView catIconIv;
    private EditText nameEt, catTypeEt, catWeightEt, descriptionEt;
    private TextView categoryTv;
    private Button updateCatBtn;
    private static final int GalleryPick = 1;

    //image picked uri
    private Uri image_uri;

    private FirebaseAuth firebaseAuth;

    private ProgressDialog progressDialog;

    private  String catID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_cat_profile);

        backBtn = findViewById(R.id.backBtn);
        catIconIv = findViewById(R.id.catIconIv);
        nameEt = findViewById(R.id.nameEt);
        catTypeEt = findViewById(R.id.catTypeEt);
        catWeightEt = findViewById(R.id.catWeightEt);
        descriptionEt = findViewById(R.id.descriptionEt);
        categoryTv = findViewById(R.id.categoryTv);
        updateCatBtn = findViewById(R.id.updateCatBtn);

        //get id of cat from intent
        catID = getIntent().getStringExtra("catID");

        firebaseAuth = FirebaseAuth.getInstance();
        loadCatDetails();

        //setup progress dialog
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Please Wait");
        progressDialog.setCanceledOnTouchOutside(false);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        catIconIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //showImagePickDialog();
                OpenGallery();
            }
        });

        categoryTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                categoryDialog();
            }
        });

        updateCatBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //input data
                inputData();
            }
        });
    }

    private void loadCatDetails() {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Users");
        reference.child(firebaseAuth.getUid()).child("Cat Profile").child(catID)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        //get data
                        String id = ""+snapshot.child("catID").getValue();
                        String catName = ""+snapshot.child("catName").getValue();
                        String catType = ""+snapshot.child("catType").getValue();
                        String catWeight = ""+snapshot.child("catWeight").getValue();
                        String catCategory = ""+snapshot.child("catCategory").getValue();
                        String catDescription = ""+snapshot.child("catDescription").getValue();
                        String catImage = ""+snapshot.child("catImage").getValue();
                        String uid = ""+snapshot.child("uid").getValue();

                        nameEt.setText(catName);
                        catTypeEt.setText(catType);
                        catWeightEt.setText(catWeight);
                        categoryTv.setText(catCategory);
                        descriptionEt.setText(catDescription);

                        try {
                            Picasso.get().load(catImage).placeholder(R.drawable.ic_default_img).into(catIconIv);
                        }
                        catch (Exception e){
                            catIconIv.setImageResource(R.drawable.ic_default_img);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
    }

    private String catName, catType, catCategory, catWeight, catDescription;
    private void inputData() {
        //input Data
        catName = nameEt.getText().toString().trim();
        catType = catTypeEt.getText().toString().trim();
        catCategory = categoryTv.getText().toString().trim();
        catWeight = catWeightEt.getText().toString().trim();
        catDescription = descriptionEt.getText().toString().trim();

        //Validate data
        if(catName.isEmpty()){
            nameEt.setError("Cat name is required");
            nameEt.requestFocus();
            return;
        }
        if(catType.isEmpty()){
            catTypeEt.setError("Cat name is required");
            catTypeEt.requestFocus();
            return;
        }
        if(catName.isEmpty()){
            nameEt.setError("Cat type is required");
            nameEt.requestFocus();
            return;
        }
        if(catWeight.isEmpty()){
            catWeightEt.setError("Cat weight is required");
            catWeightEt.requestFocus();
            return;
        }
        if(catCategory.isEmpty()){
            categoryTv.setError("Cat category is required");
            categoryTv.requestFocus();
            return;
        }
        if(catDescription.isEmpty()){
            descriptionEt.setError("Description is required");
            descriptionEt.requestFocus();
            return;
        }

        else{
            updateCat();
        }

    }

    private void updateCat() {
        progressDialog.setMessage("Updating cat...");
        progressDialog.show();

        if(image_uri == null){
            //update without image

            //setup data in hashMap to update
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("catName", ""+ catName);
            hashMap.put("catType", ""+ catType);
            hashMap.put("catWeight", ""+ catWeight);
            hashMap.put("catCategory", ""+ catCategory);
            hashMap.put("catDescription", ""+ catDescription);

            //update to db
            DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Users");
            reference.child(firebaseAuth.getUid()).child("Cat Profile").child(catID)
                    .updateChildren(hashMap)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            progressDialog.dismiss();
                            Toast.makeText(EditCatProfileActivity.this, "Updated...", Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(EditCatProfileActivity.this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
        }
        else{
            //update with image

            //first upload image
            //image name and path on firebase storage
            String filePathAndName = "product_images/" + "" + catID; //override pervious image using same ID
            //uploaded image
            StorageReference storageReference = FirebaseStorage.getInstance().getReference(filePathAndName);
            storageReference.putFile(image_uri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            //image uploaded, get url of uploaded image

                            Task<Uri> uriTask = taskSnapshot.getStorage().getDownloadUrl();
                            while (!uriTask.isSuccessful());
                            Uri downloadImageUri = uriTask.getResult();

                            if(uriTask.isSuccessful()){
                                //setup data in hashMap to update
                                HashMap<String, Object> hashMap = new HashMap<>();
                                hashMap.put("catImage", ""+ downloadImageUri);
                                hashMap.put("catName", ""+ catName);
                                hashMap.put("catType", ""+ catType);
                                hashMap.put("catWeight", ""+ catWeight);
                                hashMap.put("catCategory", ""+ catCategory);
                                hashMap.put("catDescription", ""+ catDescription);

                                //update to db
                                DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Users");
                                reference.child(firebaseAuth.getUid()).child("Cat Profile").child(catID)
                                        .updateChildren(hashMap)
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                progressDialog.dismiss();
                                                Toast.makeText(EditCatProfileActivity.this, "Updated...", Toast.LENGTH_SHORT).show();
                                            }
                                        })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                progressDialog.dismiss();
                                                Toast.makeText(EditCatProfileActivity.this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                                            }
                                        });
                            }

                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(EditCatProfileActivity.this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
        }
    }

    private void categoryDialog() {
        //dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Cat Category")
                .setItems(Category.catCategories, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String category = Category.catCategories[which];
                        categoryTv.setText(category);
                    }
                })
                .show();
    }

    private void OpenGallery() {
        Intent galleryIntent = new Intent();
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        startActivityForResult(galleryIntent, GalleryPick);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GalleryPick && resultCode == RESULT_OK && data != null && data.getData() != null) {
            image_uri = data.getData();
            catIconIv.setImageURI(image_uri);
        }
    }
}