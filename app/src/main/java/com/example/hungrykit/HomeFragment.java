package com.example.hungrykit;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;


public class HomeFragment extends Fragment {

  //  private Button addCatBtn;
    private ProgressBar progressBar1, progressBar2;
    private Button tankBtn, tank2Btn;
    private TextView percent1, percent2;
    private CardView addCatCV, tempCatCV, seTimerCV;
    private DatabaseReference databaseReference;

    private int feederAStatus = 100;
    private int feederBStatus = 0;


     @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_home, container, false);

        progressBar1 = view.findViewById(R.id.progressBar1);
        progressBar2 = view.findViewById(R.id.progressBar2);
        tankBtn = view.findViewById(R.id.tankBtn);
        tank2Btn = view.findViewById(R.id.tank2Btn);
        percent1 = view.findViewById(R.id.percent1);
        percent2 = view.findViewById(R.id.percent2);
        addCatCV = view.findViewById(R.id.addCatCV);
        tempCatCV = view.findViewById(R.id.tempCatCV);
        seTimerCV = view.findViewById(R.id.seTimerCV);

        databaseReference = FirebaseDatabase.getInstance().getReference().child("FeedMe");
        progressBar1.setMax(100);



         addCatCV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), AddCatActivity.class));
            }
        });

        tempCatCV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), TemperatureActivity.class));
            }
        });

        seTimerCV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), SetTimerActivity.class));
            }
        });


        tankBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                feederAStatus -=10;
                if(feederAStatus>=10){
                    feederAStatus -=10;
                }
                progressBar1.setProgress(feederAStatus);
                percent1.setText(feederAStatus+"%");
                feedMe();
            }
        });


        return view;
    }

    public void feedMe(){
         databaseReference.child("feederA").setValue("ON");
         databaseReference.child("feederApercent").setValue(feederAStatus);
    }
}

