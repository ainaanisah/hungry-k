package com.example.hungrykit;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import java.sql.Array;
import java.util.ArrayList;
import java.util.List;

public class AdapterCat extends RecyclerView.Adapter<AdapterCat.MyHolder>{

     Context context;
     ArrayList<ModelCat> catList;

    public AdapterCat(Context context, ArrayList<ModelCat> catList) {
        this.context = context;
        this.catList = catList;
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //inflate layout(row_cat.xml)
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_cat, parent, false);
        MyHolder myHolder = new MyHolder(view);
        return myHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, int position) {
        //get data
        ModelCat modelCat = catList.get(position);
        String id = modelCat.getCatID();
        String uid = modelCat.getUid();
        String name = modelCat.getCatName();
        String type = modelCat.getCatType();
        String icon = modelCat.getCatImage();

        //set data
        holder.mNameTv.setText(name);
        holder.mTypeTv.setText(type);

        try{
            Picasso.get().load(icon)
                    .placeholder(R.drawable.ic_default_img)
                    .into(holder.mAvatarIv);
        }
        catch (Exception e){
            holder.mAvatarIv.setImageResource(R.drawable.ic_default_img);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //handle item clicks, show item details
                detailsBottomSheet(modelCat);
            }
        });
    }

    private void detailsBottomSheet(ModelCat modelCat) {
        //bottom sheet
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(context);
        //inflate view fot bs
        View view = LayoutInflater.from(context).inflate(R.layout.bs_cat_details, null);
        bottomSheetDialog.setContentView(view);

        //show dialog
        bottomSheetDialog.show();

        //init view of bs
        ImageButton backBtn = view.findViewById(R.id.backBtn);
        ImageButton deleteBtn = view.findViewById(R.id.deleteBtn);
        ImageButton editBtn = view.findViewById(R.id.editBtn);
        ImageView catIconIv = view.findViewById(R.id.catIconIv);
        TextView nameTv = view.findViewById(R.id.nameTv);
        TextView typeTv = view.findViewById(R.id.typeTv);
        TextView weightTv = view.findViewById(R.id.weightTv);
        TextView categoryTv = view.findViewById(R.id.categoryTv);
        TextView descriptionTv = view.findViewById(R.id.descriptionTv);

        //get data
        String id = modelCat.getCatID();
        String uid = modelCat.getUid();
        String name = modelCat.getCatName();
        String type = modelCat.getCatType();
        String weight = modelCat.getCatWeight();
        String category = modelCat.getCatCategory();
        String description = modelCat.getCatDescription();
        String icon = modelCat.getCatImage();

        //set data
        nameTv.setText(name);
        typeTv.setText(type);
        weightTv.setText(weight+"kg");
        categoryTv.setText(category);
        descriptionTv.setText(description);
        try {
            Picasso.get().load(icon).placeholder(R.drawable.ic_default_img).into(catIconIv);
        }
        catch (Exception e){
            catIconIv.setImageResource(R.drawable.ic_default_img);
        }

        bottomSheetDialog.show();

        //edit click
        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //open edit activity
                Intent intent = new Intent(context, EditCatProfileActivity.class);
                intent.putExtra("catID", id);
                context.startActivity(intent);
            }
        });

        //delete click
        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //show delete activity
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Delete")
                        .setMessage("Are sure you want to delete " +name+" ?")
                        .setPositiveButton("DELETE", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //delete cat
                                deleteCat(id);

                            }
                        }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //cancel, dismiss
                        dialog.dismiss();
                    }
                })
                 .show();
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.dismiss();
            }
        });

    }

    private void deleteCat(String id) {
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Users");
        reference.child(firebaseAuth.getUid()).child("Cat Profile").child(id).removeValue()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        //cat deleted
                        Toast.makeText(context, "Cat deleted...", Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        //failed delete cat
                        Toast.makeText(context, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public int getItemCount() {

        return catList.size();
    }

    //view holder class
    class MyHolder extends RecyclerView.ViewHolder{

        private ImageView mAvatarIv;
        private TextView mNameTv, mTypeTv;


        public MyHolder(@NonNull View itemView) {
            super(itemView);

            //init views
            mAvatarIv = itemView.findViewById(R.id.avatarIv);
            mNameTv = itemView.findViewById(R.id.nameTv);
            mTypeTv = itemView.findViewById(R.id.typeTv);
        }
    }
}
