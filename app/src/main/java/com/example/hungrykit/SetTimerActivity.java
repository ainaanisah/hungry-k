package com.example.hungrykit;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class SetTimerActivity extends AppCompatActivity {

    Button feederABtn, feederBBtn;
    ImageButton backBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_timer);


        backBtn = findViewById(R.id.backBtn);
        feederABtn = findViewById(R.id.feederABtn);
        feederBBtn = findViewById(R.id.feederBBtn);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        feederABtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SetTimerActivity.this, feederAtime.class));
            }
        });

        feederBBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SetTimerActivity.this, feederBtime.class));
            }
        });

    }
}

