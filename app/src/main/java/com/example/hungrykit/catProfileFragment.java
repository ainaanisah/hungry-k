package com.example.hungrykit;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.MenuItemCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;


import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


public class catProfileFragment extends Fragment {

    RecyclerView cat_recyclerView;
    ArrayList<ModelCat> catList;
    AdapterCat adapterCat;

   private FirebaseAuth firebaseAuth;

    public catProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        //inflater.inflate(R.menu.menu_main, menu);

        //search view
        MenuItem item = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);

        //search listener
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                //called when user press search button from keyboard
                //if search query is not empty then search
                if(!TextUtils.isEmpty(query.trim())){
                    //search text contains text, search it
                    searchCat(query);
                }else{
                    //search text empty, get all cats
                    loadAllCat();
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                //called whenever user press any single letter
                if(!TextUtils.isEmpty(newText.trim())){
                    //search text contains text, search it
                    searchCat(newText);
                }else{
                    //search text empty, get all cats
                    loadAllCat();
                }
                return false;
            }
        });

        super.onCreateOptionsMenu(menu, inflater);
    }

    private void searchCat(String query) {
        //get all cat
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Users");
        reference.child(firebaseAuth.getUid()).child("Cat Profile")
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        //before getting reset list
                        catList.clear();
                        for(DataSnapshot ds: snapshot.getChildren()){
                            ModelCat modelCat = ds.getValue(ModelCat.class);

                            if(modelCat.getCatName().toLowerCase().contains(query.toLowerCase()) ||
                                modelCat.getCatCategory().toLowerCase().contains(query.toLowerCase())){
                                catList.add(modelCat);
                            }

                        }
                        //setup Adapter
                        adapterCat = new AdapterCat(getActivity(), catList);
                        //refresh adapter
                        adapterCat.notifyDataSetChanged();
                        //set adapter
                        cat_recyclerView.setAdapter(adapterCat);

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_cat_profile, container, false);

        cat_recyclerView = view.findViewById(R.id.cat_recyclerView);


        //set it's properties
        cat_recyclerView.setHasFixedSize(true);
        //  cat_recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        cat_recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));


        //init cat list
        catList = new ArrayList<>();


        firebaseAuth = FirebaseAuth.getInstance();

        //getAll cat
        loadAllCat();

        return view;
    }

    private void loadAllCat() {
        catList = new ArrayList<>();

        //get all cat
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Users");
        reference.child(firebaseAuth.getUid()).child("Cat Profile")
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        //before getting reset list
                        catList.clear();
                        for(DataSnapshot ds: snapshot.getChildren()){
                            ModelCat modelCat = ds.getValue(ModelCat.class);
                            catList.add(modelCat);
                        }
                        //setup Adapter
                        adapterCat = new AdapterCat(getActivity(), catList);

                        //set adapter
                        cat_recyclerView.setAdapter(adapterCat);

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
    }
}