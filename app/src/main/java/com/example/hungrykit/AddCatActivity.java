package com.example.hungrykit;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.HashMap;

public class AddCatActivity extends AppCompatActivity {

    private ImageButton backBtn;
    private ImageView catIconIv;
    private EditText nameEt, catTypeEt, catWeightEt, descriptionEt;
    private TextView categoryTv;
    private Button addCatBtn;
    private static final int GalleryPick = 1;

    //permission constant
    private static final int CAMERA_REQUEST_CODE = 200;
    private static final int STORAGE_REQUEST_CODE = 300;
    //image pick constant
    private static final int IMAGE_PICK_GALLERY_CODE = 400;
    private static final int IMAGE_PICK_CAMERA_CODE = 500;
    //permission arrays
    private String[] cameraPermissions;
    private String[] storagePermissions;
    //image picked uri
    private Uri image_uri;

    private FirebaseAuth firebaseAuth;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_cat);

        backBtn = findViewById(R.id.backBtn);
        catIconIv = findViewById(R.id.catIconIv);
        nameEt = findViewById(R.id.nameEt);
        catTypeEt = findViewById(R.id.catTypeEt);
        catWeightEt = findViewById(R.id.catWeightEt);
        descriptionEt = findViewById(R.id.descriptionEt);
        categoryTv = findViewById(R.id.categoryTv);
        addCatBtn = findViewById(R.id.addCatBtn);

        firebaseAuth = FirebaseAuth.getInstance();

        //setup progress dialog
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Please Wait");
        progressDialog.setCanceledOnTouchOutside(false);

        //init permission arrays
        cameraPermissions = new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};
        storagePermissions = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        catIconIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //showImagePickDialog();
                OpenGallery();
            }
        });

        categoryTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                categoryDialog();
            }
        });
        
        addCatBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //input data
                inputData();
            }
        });

    }

    private String catName, catType, catCategory, catWeight, catDescription;
    private void inputData() {
        //input Data
        catName = nameEt.getText().toString().trim();
        catType = catTypeEt.getText().toString().trim();
        catCategory = categoryTv.getText().toString().trim();
        catWeight = catWeightEt.getText().toString().trim();
        catDescription = descriptionEt.getText().toString().trim();

        //Validate data
        if(catName.isEmpty()){
            nameEt.setError("Cat name is required");
            nameEt.requestFocus();
            return;
        }
        if(catType.isEmpty()){
            catTypeEt.setError("Cat name is required");
            catTypeEt.requestFocus();
            return;
        }
        if(catName.isEmpty()){
            nameEt.setError("Cat type is required");
            nameEt.requestFocus();
            return;
        }
        if(catWeight.isEmpty()){
            catWeightEt.setError("Cat weight is required");
            catWeightEt.requestFocus();
            return;
        }
        if(catCategory.isEmpty()){
            categoryTv.setError("Cat category is required");
            categoryTv.requestFocus();
            return;
        }
        if(catDescription.isEmpty()){
            descriptionEt.setError("Description is required");
            descriptionEt.requestFocus();
            return;
        }
        else{
            addCat();
        }

    }

    private void addCat() {
        //add cat to db
        progressDialog.setMessage("Adding Cat...");
        progressDialog.show();

        String timestamp = ""+System.currentTimeMillis();

        if (image_uri == null){
            //upload without image
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("catID", ""+timestamp);
            hashMap.put("catName", ""+catName);
            hashMap.put("catType", ""+catType);
            hashMap.put("catWeight", ""+catWeight);
            hashMap.put("catCategory", ""+catCategory);
            hashMap.put("catDescription", ""+catDescription);
            hashMap.put("catImage", ""); // no image, set empty
            hashMap.put("uid", ""+firebaseAuth.getUid());

            //add to db
            DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Users");
            reference.child(firebaseAuth.getUid()).child("Cat Profile").child(timestamp).setValue(hashMap)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            progressDialog.dismiss();
                            Toast.makeText(AddCatActivity.this, "Cat added", Toast.LENGTH_SHORT).show();
                            clearData();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(AddCatActivity.this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
        }
        else{
            //upload with image

            //name and path of image to be upload
            String filePathAndName = "product_images/" + "" + timestamp;

            StorageReference storageReference = FirebaseStorage.getInstance().getReference(filePathAndName);
            storageReference.putFile(image_uri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            //image uploaded
                            //get url of uploaded image
                            Task<Uri> uriTask = taskSnapshot.getStorage().getDownloadUrl();
                            while(!uriTask.isSuccessful());
                            Uri downloadImageUri = uriTask.getResult();

                            if(uriTask.isSuccessful()){
                                //url og image received, upload to db
                                HashMap<String, Object> hashMap = new HashMap<>();
                                hashMap.put("catID", ""+timestamp);
                                hashMap.put("catName", ""+catName);
                                hashMap.put("catType", ""+catType);
                                hashMap.put("catWeight", ""+catWeight);
                                hashMap.put("catCategory", ""+catCategory);
                                hashMap.put("catDescription", ""+catDescription);
                                hashMap.put("catImage", ""+downloadImageUri);
                                hashMap.put("uid", ""+firebaseAuth.getUid());

                                //add to db
                                DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Users");
                                reference.child(firebaseAuth.getUid()).child("Cat Profile").child(timestamp).setValue(hashMap)
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                progressDialog.dismiss();
                                                Toast.makeText(AddCatActivity.this, "Cat added", Toast.LENGTH_SHORT).show();
                                                clearData();
                                            }
                                        })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                progressDialog.dismiss();
                                                Toast.makeText(AddCatActivity.this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                                            }
                                        });
                            }

                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(AddCatActivity.this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
        }

    }

    private void clearData(){
        //clear data after uploading cat
        nameEt.setText("");
        catTypeEt.setText("");
        catWeightEt.setText("");
        categoryTv.setText("");
        descriptionEt.setText("");
        catIconIv.setImageResource(R.drawable.ic_default_img);
        image_uri = null;

    }

    private void categoryDialog() {
        //dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Cat Category")
                .setItems(Category.catCategories, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String category = Category.catCategories[which];
                        categoryTv.setText(category);
                    }
                })
                .show();
    }
    private void OpenGallery() {
        Intent galleryIntent = new Intent();
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        startActivityForResult(galleryIntent, GalleryPick);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GalleryPick && resultCode == RESULT_OK && data != null && data.getData() != null) {
            image_uri = data.getData();
            catIconIv.setImageURI(image_uri);
        }
    }

//    private void showImagePickDialog() {
//        String[] options = {"Camera", "Gallery"};
//        //dialog
//        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.setTitle("Pick Image")
//                .setItems(options, new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        //handle item clicked
//                        if(which == 0){
//                            //camera clicked
//                            if(checkCameraPermission()){
//                                //permission granted
//                                pickFromCamera();
//                            }
//                            else{
//                                //permission not granted
//                                requestCameraPermission();
//                            }
//                        }
//                        else{
//                            //gallery clicked
//                            if(checkStoragePermission()){
//                                pickFromGallery();
//                            }
//                            else{
//                                requestStoragePermission();
//                            }
//                        }
//
//                    }
//                })
//                .show();
//    }
//
//    private void pickFromGallery(){
//        //intent to pick image from gallery
//        Intent intent = new Intent(Intent.ACTION_PICK);
//        intent.setType("image/*");
//        startActivityForResult(intent, IMAGE_PICK_GALLERY_CODE);
//    }
//
//    private void pickFromCamera(){
//        ContentValues contentValues = new ContentValues();
//        contentValues.put(MediaStore.Images.Media.TITLE, "Temp_Image_Title");
//        contentValues.put(MediaStore.Images.Media.DESCRIPTION, "Temp_Image_Description");
//
//        image_uri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);
//
//        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        intent.putExtra(MediaStore.EXTRA_OUTPUT, image_uri);
//        startActivityForResult(intent, IMAGE_PICK_CAMERA_CODE);
//    }
//
//    private boolean checkStoragePermission(){
//        boolean result = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
//                (PackageManager.PERMISSION_GRANTED);
//        return result;
//    }
//
//    private  void requestStoragePermission(){
//        ActivityCompat.requestPermissions(this, storagePermissions, STORAGE_REQUEST_CODE);
//    }
//
//    private boolean checkCameraPermission(){
//        boolean result = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) ==
//                (PackageManager.PERMISSION_GRANTED);
//        boolean result1 = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
//                (PackageManager.PERMISSION_GRANTED);
//        return result && result1;
//
//    }
//
//    private  void requestCameraPermission(){
//        ActivityCompat.requestPermissions(this, cameraPermissions, CAMERA_REQUEST_CODE);
//    }
//
//    //handle permission results
//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        switch (requestCode){
//            case CAMERA_REQUEST_CODE:{
//                if(grantResults.length>0){
//                    boolean cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
//                    boolean storageAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;
//                    if(cameraAccepted && storageAccepted){
//                        //both permission granted
//                        pickFromCamera();
//                    }
//                    else{
//                        //both or one permission granted
//                        Toast.makeText(this, "Camera & Storage permission are required", Toast.LENGTH_SHORT).show();
//                    }
//                }
//            }
//            case STORAGE_REQUEST_CODE:{
//                if(grantResults.length>0){
//                    boolean storageAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
//                    if(storageAccepted){
//                        //permission granted
//                        pickFromGallery();
//                    }else{
//                        //permission denied
//                        Toast.makeText(this, "Storage permission is required", Toast.LENGTH_SHORT).show();
//                    }
//                }
//            }
//        }
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//    }
//
//    //handle image pick results
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        if (requestCode == RESULT_OK){
//            if(requestCode == IMAGE_PICK_GALLERY_CODE){
//                //image picked from gallery
//
//                //save picked image uri
//                image_uri = data.getData();
//
//                //set image
//                catIconIv.setImageURI(image_uri);
//            }
//            else if (requestCode == IMAGE_PICK_CAMERA_CODE){
//                //image picked from camera
//
//                catIconIv.setImageURI(image_uri);
//            }
//        }
//        super.onActivityResult(requestCode, resultCode, data);
//    }
}